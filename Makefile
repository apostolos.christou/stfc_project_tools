#
BIN =  /Users/aac/projects/COORBITALS/MARS/survey
HEAD = -I/Users/aac/projects/COORBITALS/MARS/survey
#
#
C =  ${HEAD}
#C = -g ${HEAD}

SOURCES=  julian.c\
           vector.c\
           planet.c\
           sun.c\
	   moon.c\
           asteroid.c

OBJECTS =  ${SOURCES:.c=.o}

ephem2  :   ${OBJECTS}       
	gcc $C -o ephem2 ${OBJECTS} driver2.c -lm 

ephem3  :   ${OBJECTS}
	gcc $C -o ephem3 ${OBJECTS} driver3.c -lm 

.c.o:
	gcc $C -c $<

clean:
	/bin/rm -f *.o *.trace core
