
  
  #include "ephem.h"
 
 /*                          
  struct vector lincomb(double a, struct vector v1, double b, struct vector v2);                         
  struct vector add(struct vector v1,struct vector v2);
  struct vector subtract(struct vector v1,struct vector v2);
  double dot(struct vector vec1, struct vector vec2);  
  double norm2(struct vector vec);
  struct vector cross(struct vector vec1, struct vector vec2);            
                      
                      */
                                        
  struct vector lincomb(double a, struct vector v1, double b, struct vector v2)
  {
  struct vector res;
  
  res.x=a*(v1.x) + b*(v2.x); 
  res.y=a*(v1.y) + b*(v2.y);
  res.z=a*(v1.z) + b*(v2.z); 
  
  return(res);
  }


  struct vector add(struct vector v1,struct vector v2)
  {            
  static double a=1.0;
  static double b=1.0;
  return(lincomb(a,v1,b,v2));
  }
  
   struct vector subtract(struct vector v1,struct vector v2)
  {            
  static double a=1.0;
  static double b=-1.0;
  return(lincomb(a,v1,b,v2));
  }
                                
  double dot(struct vector vec1, struct vector vec2)
  {
  return((vec1.x)*(vec2.x) + (vec1.y)*(vec2.y) + (vec1.z)*(vec2.z));            
  }
  
  double norm2(struct vector vec)
  {
  return(dot(vec,vec));
  }
  
  struct vector cross(struct vector vec1, struct vector vec2)
  {
  struct vector res;
  
  res.x=(vec1.y)*(vec2.z) - (vec1.z)*(vec2.y);
  res.y=(vec1.z)*(vec2.x) - (vec1.x)*(vec2.z);
  res.z=(vec1.x)*(vec2.y) - (vec1.y)*(vec2.x);
  
  return(res);
  }  

void BubbleSort(double a[], int array_size)
{
  int i, j;
  double temp;

  for (i = 0; i < (array_size - 1); ++i)
    {
      for (j = 0; j < array_size - 1 - i; ++j )
	{
	  if (a[j] > a[j+1])
	    {
	      temp = a[j+1];
	      a[j+1] = a[j];
	      a[j] = temp;
	    }
	}
    }
}   
