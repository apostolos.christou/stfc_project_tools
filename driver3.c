#include"ephem.h"    

      struct planet venus=
      {
        { 324858.63},
                            
        { 0.72333199, 0.00677323, 3.39471, 76.68069,           
          131.53298, 181.97973},
          
        { 0.00000092, -0.00004938, -2.86,-996.89,
         -108.80, 210664136.06}  
      };
                       

       struct planet earth=
      {
        { 398600.44},
        
          {1.00000011, 0.01671022, 0.00005, -11.26064,
           102.94719,  100.46435},
           
          {-0.00000005, -0.00003804, -46.94, -18228.25,
            1198.28, 129597740.63}
      };                

      struct planet mars=
      { 
        { 42828.3},
        
          {1.52366231, 0.09341233,1.85061, 49.57854, 
           336.04084, 355.45332},
           
          {-0.00007221, 0.00011902, -25.47, -1020.19,
            1560.78, 68905103.78}
      };      
           

/*   JPL elements for epoch 2458200.5  */


struct planet eureka=
{
    { 42828.3},
 /*   om, om+w, om+w+ma  */
    {1.523469648751129,0.06462718469405342, 20.28131313524494, 245.1262505503460,
        340.566938192092, 296.895939142829},
    {-0.00007221, 0.00011902, -25.47, -1020.19,
        1560.78, 68905103.78}
};

struct planet vf31=
{
    { 42828.3},
    
    {1.524089882166990	, .1005931677554052	,31.29756942330895, 221.3778728042990,
        171.823067309375, 286.309321401405},
    
    {-0.00007221, 0.00011902, -25.47, -1020.19,
        1560.78, 68905103.78}
};



struct planet uj7=
{
    { 42828.3},
    
    {1.524428577292700, .03924676085059516,16.75022057270314, 347.4514667807587,
        35.5752983884326, 79.5243955740608},
    
    {-0.00007221, 0.00011902, -25.47, -1020.19,
        1560.78, 68905103.78}
};

         struct planet sun=
      {
        { 1.32725e11},
          {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
          {0.0, 0.0, 0.0, 0.0, 0.0, 0.0}
      };      

          double eps=1e-3;


    int main()
        {
        int Year_1,  Month_1, Day_1;
        int Hours_1, Mins_1,  Secs_1;
        int Year_2,  Month_2, Day_2;
        int Hours_2, Mins_2,  Secs_2;
        int Year, Month, Day;
        int n=0, nsteps, ttstep, jobs, jlan, jper;
        int ilan=30, iper=30, iobs=100, itrials=1000;
            int idtct, i, irate;
        int m,nmbr;
        time_t seconds;
        double tt, tstep_days, ttstart, ttend, dtime, rem;
        double ph, mag, elng, pd;
        double r0, r, trialv0, trialv;
        double r1, r2, trialv1, trialv2;
        double h, hmin, hmax, dh=0.1, h0;
        double a0;
        double rate,cdf_num=0e0,cdf_dnm=0e0;
/*        double thres=4e0;   for H=20 */
        double thres=15e-1; /* for H=20.5 */
 /*      double thres=9e0;   for H=19.5 */
        double family[10001];
        struct vector lun;
        struct planet ast;
            
            for(n=0;n<10000;n++)
             {
                 family[n]=9.99e2;
             }
            
            n=0;
            
            ast.phys.gm=mars.phys.gm;

                ast.el.a=eureka.el.a;
                ast.el.e=eureka.el.e;
                ast.el.i=eureka.el.i;
                ast.el.lan=eureka.el.lan;
                ast.el.lper=eureka.el.lper;
                ast.el.ml=eureka.el.ml;
            
                ast.del.da=eureka.del.da;
                ast.del.de=eureka.del.de;
                ast.del.di=eureka.del.di;
                ast.del.dlan=eureka.del.dlan;
                ast.del.dlper=eureka.del.dlper;
                ast.del.dml=eureka.del.dml;
            
            /*
            
            
            ast.el.a=vf31.el.a;
            ast.el.e=vf31.el.e;
            ast.el.i=uj7.el.i;
            ast.el.lan=vf31.el.lan;
            ast.el.lper=vf31.el.lper;
            ast.el.ml=vf31.el.ml;
            
            ast.del.da=vf31.del.da;
            ast.del.de=vf31.del.de;
            ast.del.di=vf31.del.di;
            ast.del.dlan=vf31.del.dlan;
            ast.del.dlper=vf31.del.dlper;
            ast.del.dml=vf31.del.dml;
            
            */
            
            /*
            ast.el.a=uj7.el.a;
            ast.el.e=uj7.el.e;
            ast.el.i=uj7.el.i;
            ast.el.lan=uj7.el.lan;
            ast.el.lper=uj7.el.lper;
            ast.el.ml=uj7.el.ml;
            
            ast.del.da=uj7.del.da;
            ast.del.de=uj7.del.de;
            ast.del.di=uj7.del.di;
            ast.del.dlan=uj7.del.dlan;
            ast.del.dlper=uj7.del.dlper;
            ast.del.dml=uj7.del.dml;
            */
            
            seconds=time(NULL);  /* initate rnd generator */
            srand(seconds);
 /*       printf("%d\n",(int)seconds);   */

        
            /* reading in start & end dates */
            
        printf("GIVE START DATE: ");     
        scanf("%d/%d/%d",&Year_1,&Month_1,&Day_1);   
/*        
        printf("GIVE START TIME(UT): ");
        scanf("%d:%d:%d",&Hours_1,&Mins_1,&Secs_1);
*/
            Hours_1=12;
            Mins_1 = 0;
            Secs_1 = 0;
            
        printf("GIVE END DATE: ");     
        scanf("%d/%d/%d",&Year_2,&Month_2,&Day_2);
/*
        printf("GIVE END TIME(UT): ");
        scanf("%d:%d:%d",&Hours_2,&Mins_2,&Secs_2);

        printf("GIVE TIME STEP(DAYS): ");
        scanf("%d",&ttstep);
           */
            
            Hours_2=12;
            Mins_2 = 0;
            Secs_2 = 0;
         
/*        printf("OK so far\n");    */
        ttstart=julian(Year_1, Month_1, Day_1, Hours_1, Mins_1, Secs_1);       
        ttend=julian(Year_2, Month_2, Day_2, Hours_2, Mins_2, Secs_2);
        /*
        tstep_days=(double)ttstep;
         */
            
    	dtime=ttend-ttstart;
            
        /*
        rem=modf(dtime/tstep_days, &steps);
    
        rem*=ttstep;
        */
            
        /*
        printf("%lf %lf %d\n", tstep_days,time,nsteps);
        */
           /*
            hmin=15.0;
            hmax=24.0;
            h=hmin;
            */
            
            /* Calculating number of asteroids with
                 a particular size distribution up to HMAX  */
            
            h0=H0;
            
            a0=-h0*B0;
      nsteps=pow(10,a0+HMAX*B0);
            printf("%d \n",nsteps);
         /*   printf("%d \n", nsteps);  */
     
     for(i=0;i<itrials;i++)
     {
     nmbr=0;
     while(n<nsteps)
     {
         idtct=0;
          /*  ast.el.ml=mars.el.ml-6e1; for Eureka family */
        /*  ast.el.ml=mars.el.ml-7e1; for VF31 family */
         /*   ast.el.ml=mars.el.ml+8e1; for UJ7 family */
      /*   ast.el.i=30e0;  */
        
          /*  calendar(ttstart, &Year, &Month, &Day); */
 /*
         r=rand();
         trialv=(float)r/(float)RAND_MAX;
         tt=ttstart+(ttend-ttstart)*trialv;
         lun=el2pos(&tt,ast);
         lun=subtract(lun,el2pos(&tt,earth));
         ph=phase(&tt,ast,earth);
 */
         /*calc visual magnitude for random H) */
         r0=rand();
         trialv0=(float)r0/(float)RAND_MAX;
         h=mag_d(&trialv0,nsteps,h0,B0);
         family[n]=h;
/*
         mag=h+vmag(&tt,&ph,ast,earth);
         elng=elong(&tt,ast,earth);
         pd=pdtct(&mag);
         r=rand();
         trialv=(float)r/(float)RAND_MAX;
         if(trialv<pd && elng>70e0) idtct+=1;
         printf("%lf %d\n",h,idtct);
*/
 /*
     printf("%d %d %d %lf %lf %d\n",Year,Month,Day,mag,pd,idtct); 
       */
/*         for(jlan=0;jlan<ilan;jlan++)  remove if wish to reinstate w, om, averaging */
        /* randomizing om and w */
        /*   { remove if wish to reinstate w, om, averaging */
             r1=rand();
             trialv1=(float)r1/(float)RAND_MAX;
             ast.el.lan=trialv1*3.6e2;
        /* for(jper=0;jper<iper;jper++)
           { remove if wish to reinstate w, om, averaging */
             r2=rand();
             trialv2=(float)r2/(float)RAND_MAX;
             ast.el.lper=trialv2*3.6e2;
      /*
         for(j=0;j<nsteps;j++)
         {
         tt=ttstart+(double)(j+1)*tstep_days;
       */
        for(jobs=0;jobs<iobs;jobs++)
            /* generating anumbe rof random observations */
        {
        r=rand();
        trialv=(float)r/(float)RAND_MAX;
        tt=ttstart+(ttend-ttstart)*trialv;
         /*
         lun=lunpos(&tt);
         */
        /* calendar(tt, &Year, &Month, &Day);  */
         lun=el2pos(&tt,ast);
         lun=subtract(lun,el2pos(&tt,earth));
         ph=phase(&tt,ast,earth);
             mag=h+vmag(&tt,&ph,ast,earth);
             elng=elong(&tt,ast,earth);
             pd=pdtct(&mag);
             r=rand();
             trialv=(float)r/(float)RAND_MAX;
             if(trialv<pd && elng>70e0) idtct+=1;
           }
 /*          printf("%d %d %d %lf %lf %d\n",Year,Month,Day,mag,pd,idtct);   */
     /*
    if (rem>TOL)
       {    
      tt+=rem/8.64e4;
      calendar(tt, &Year, &Month, &Day);
      lun=el2pos(&tt,ast);
      lun=subtract(lun,el2pos(&tt,earth));
           ph=phase(&tt,ast,earth);
           mag=h+vmag(&tt,&ph,ast,earth);
           elng=elong(&tt,ast,earth);
           pd=pdtct(&mag);
           r=rand();
           trialv=(float)r/(float)RAND_MAX;
           if(trialv<pd && elng>70e0) idtct+=1;
      */
/*      printf("%d %d %d %lf %lf %d\n",Year,Month,Day,mag,pd,idtct); */
/*           printf("%lf %d\n",h,idtct);    */
      /*
           lun=vel_ephem(&tt,eps,mars);
      */
    /* }} remove if wish to reinstate w, om, averaging */
        /* irate=6; */
         rate=(double)idtct/(double)(ilan*iper);
         rate=(double)idtct;
         if (rate>=thres)
         {
         nmbr+=1;
         /*
         printf("%lf  %d  %lf\n",h,idtct,rate);
         */
         }
  /*
        irate=6;
         cdf_num+=poisson_k(rate,irate);
         cdf_dnm+=1e0;
            printf("%lf %lf %lf\n",h,poisson_k(rate,irate),cdf_num/cdf_dnm);
   */
            /*        printf("%lf %lf\n",h, poisson_k(rate,irate));   */
         n++;
     }
         printf("%d  %d\n",i,nmbr);
         
         for(m=0;m<10000;m++)
         {
             family[m]=9.99e2;
         }
         
         n=0;
     }
             /*
            BubbleSort(family,nsteps);
              */
            /*
            for(n=0;n<nsteps;n++)
            {
                printf("%lf\n",family[n]);
            }
            */
          
  }

