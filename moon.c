      #include"ephem.h"
 

      void corr(double *time, double *tmat)
      
       /* TRANSFORMS ECLIPTIC TO EQUATORIAL COORDINATES */
       
      {
      double eps=(5029.0966*S2R)*((*time) - J2000)/JULCEN;

      tmat[0]=cos(eps);
      tmat[1]=sin(eps);
      tmat[2]=0.0;
      tmat[3]=-sin(eps);
      tmat[4]=cos(eps);
      tmat[5]=0.0;
      tmat[6]=0.0;
      tmat[7]=0.0;
      tmat[8]=1.0;
      }
               

            
         struct vector sph2xyz(struct spherical rlf)
         { 
         struct vector res;
         
         res.x=(rlf.r)*cos(rlf.lmda)*cos(rlf.phi);
         res.y=(rlf.r)*sin(rlf.lmda)*cos(rlf.phi);
         res.z=(rlf.r)*sin(rlf.phi);
         
         return(res);
         
         }   
         
         struct spherical luneph(double *time)  
         {         
         double t,t2,t3,t4;       
         double DCON,LCON,LDCON,FCON,OMCON,CLCON,VECON,CTCON;
         double D,L,LD,F,OM,CL,VE,CT;        
         struct spherical res;
         
         t=((*time)-J2000)/JULCEN;
         
         DCON= 297.0*D2R + 51.0*M2R + 0.73512*S2R;
         LCON= 134.0*D2R + 57.0*M2R + 48.28096*S2R;
         LDCON=357.0*D2R + 31.0*M2R + 44.79306*S2R;
         FCON= 93.0*D2R + 16.0*M2R + 19.55755*S2R;
         OMCON=125.0*D2R + 2.0*M2R + 40.39816*S2R;
         CLCON=218.0*D2R + 18.0*M2R + 59.95571*S2R;
         VECON=181.0*D2R + 58.0*M2R + 47.28305*S2R;
         CTCON=100.0*D2R + 27.0*M2R + 59.22059*S2R;
         
         t2=t*t;
         t3=t2*t;
         t4=t3*t;
        
         D=DCON + (1602961601.4603*t - 5.8681*t2  + 0.006595*t3 - 0.00003184*t4)*S2R;
         L=LCON + (1717915923.4728*t + 32.3893*t2  + 0.051651*t3 - 0.0002447*t4)*S2R;     
         LD=LDCON + (129596581.0474*t - 0.5529*t2 + 0.000147*t3)*S2R;
         F=FCON + (1739527263.0983*t - 12.2505*t2 - 0.001021*t3 + 0.00000417*t4)*S2R;
         OM=OMCON + (- 6962890.2656*t + 7.4742*t2 + 0.007702*t3 - 0.00005939*t4)*S2R;
         CL=CLCON + (1732564372.83264*t - 4.7763*t2 + 0.006681*t3 - 0.00005522*t4)*S2R;
         VE=VECON + (210664136.43355*t)*S2R;
         CT=CTCON + (129597742.2758*t - 0.0202*t2 + 0.000009*t3 + 0.00000015*t4)*S2R;
         
         res.r=3.85001e5 - 20905.0*cos(L) -3699.0*cos(2.0*D - L) - 2956.0*cos(2.0*D) -
           570.0*cos(2.0*L) + 246.0*cos(2.0*(D-L)) -205.0*cos(2.0*D - LD) -
           171.0*cos(2.0*D + L)- 152.0*cos(2.0*D - L - LD) -130.0*cos(LD - L) +
           108.0*cos(D) + 105.0*cos(L + LD) + 80.0*cos(L -2.0*F) + 49.0*cos(LD) -
           35.0*cos(4.0*D - L) + 31.0*cos(2.0*D + LD) + 24.0*cos(2.0*D + LD - L) -
           23.0*cos(3.0*L) - 22.0*cos(4.0*D - 2.0*L) -17.0*cos(D + LD) + 14.0*cos(2.0*D - 3.0*L) -
           13.0*cos(2.0*D - LD + L) - 12.0*cos(4.0*D) - 10.0*cos(2.0*(D + L)) +
           10.0*cos(2.0*(D - F)) + 10.0*cos(2.0*D - LD - 2.0*L) - 10.0*cos(2.0*(D - LD));
           
        res.lmda=CL + (22640.0*sin(L) + 4586.0*sin(2.0*D - L) + 2370.0*sin(2.0*D) +
             769.0*sin(2.0*L) - 666.0*sin(LD) - 412.0*sin(2.0*F) + 212.0*sin(2.0*(D - L)) +
             205.0*sin(2.0*D - L -LD) +192.0*sin(2.0*D + L) + 165.0*sin(2.0*D - LD) -
             147.0*sin(LD - L) - 125.0*sin(D) - 109.0*sin(LD+L)+ 55.0*sin(2.0*(D-F)) -
              45.0*sin(L + 2.0*F) + 40.0*sin(L - 2.0*F) +38.0*sin(4.0*D - L) +
              36.0*sin(3.0*L) + 31.0*sin(4.0*D - 2.0*L) -28.0*sin(2.0*D - L + LD) -
              24.0*sin(2.0*D + LD) - 19.0*sin(D - L) + 18.0*sin(D+LD) + 
              15.0*sin(2.0*D - LD + L) +14.0*sin(2.0*(D + L)) + 14.0*sin(4.0*D) +
              13.0*sin(2.0*D - 3.0*L) - 10.0*sin(LD - 2.0*L) + 
              14.0*sin(18.0*VE - 16.0*CT - L + 26.54261*D2R) -9.0*sin(2.0*(D+F) - L) +
              9.0*sin(2.0*(D -L) - LD) - 8.0*sin(D + L) + 8.0*sin(2.0*(D - LD)) -
              8.0*sin( LD + 2.0*L) - 7.0*sin(2.0*LD) + 7.0*sin(2.0*(D - LD) - L) -
              6.0*sin(2.0*(D - F) + L) - 6.0*sin(2.0*(D - F)))*S2R;
             
        res.phi=(18461.0*sin(F) + 1010.0*sin(F + L) +1.0e3*sin(L - F) + 624.0*sin(2.0*D - F) +
            199.0*sin(2.0*D -L +F) + 167.0*sin(2.0*D -L - F) +117.0*sin(2.0*D + F)+
            62.0*sin(2.0*L + F) + 33.0*sin(2.0*D + L - F) + 32.0*sin(2.0*L  - F) + 30.0*sin(2.0*D - LD - F) +
            16.0*sin(2.0*(D - L) - F) +15.0*sin(2.0*D + L + F) - 12.0*sin(2.0*D + LD - F))*S2R;    
           
           
        res.lmda=fmod(res.lmda,2.0*PI);
        res.phi=fmod(res.phi,2.0*PI);    
        
        return(res);
        
       } 
            
        struct vector lunpos(double *tt) 
        {
        struct spherical lunrlf;  
        struct vector xyz, xyz2, moon;
        double prec[9],prec2[9];
        
        lunrlf=luneph(tt);    
        xyz2=sph2xyz(lunrlf);     
        corr(tt,prec2);
        xyz.x=(xyz2.x)*prec2[0] + xyz2.y*prec2[1] + xyz2.z*prec2[2];
        xyz.y=(xyz2.x)*prec2[3] + xyz2.y*prec2[4] + xyz2.z*prec2[5];
        xyz.z=(xyz2.x)*prec2[6] + xyz2.y*prec2[7] + xyz2.z*prec2[8];
        ecl2eq(prec);
        moon.x=(xyz.x)*prec[0] + xyz.y*prec[1] + xyz.z*prec[2];
        moon.y=(xyz.x)*prec[3] + xyz.y*prec[4] + xyz.z*prec[5];
        moon.z=(xyz.x)*prec[6] + xyz.y*prec[7] + xyz.z*prec[8]; 
        
        return(moon);
        
        }    
            
