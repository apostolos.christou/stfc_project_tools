#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stddef.h>
#include <string.h>
#include <math.h>
    /* checks out 31/03/2018  */

  #define  V0            2e1
  #define  W0            2e-1
/*  #define  K0            0.292e0  */

  #define  H0            18.1e0
  #define  H00           16.1e0
  #define  HMAX          24e0
  #define  A0           -8.09967e0 /* w/o Eureka */
  #define  B0            0.446702
  #define  A00          -4.9779e0 /* w Eureka */
  #define  B00           0.292221e0


      double pdtct(double *v)
       {
           return(1e0/(1e0+exp(((*v)-V0)/W0)));
       }

/*
      double size_d(double *x)
       {
           return(pow(C0*(1.-(*x)),(1./(1-K0))));
       }
*/

       double mag_d(double *x, double nast, double HLR, double K0)
       {
      return(HLR + log10(1e0+(*x)*(nast))/K0);
       }

int main()
        {
        int nsteps=100, j,r1,r2;
        time_t seconds;
        double trialv1,trialv2;

            seconds=time(NULL);
            srand(seconds);
            
            nsteps=pow(10,A0+HMAX*B0);
 /*       printf("%d\n",(int)seconds);       */
            
     
        /*
        printf("%lf %lf %d\n", tstep_days,time,nsteps);
        */

      for(j=0;j<nsteps;j++)
         {
             r1=rand();
             r2=rand();
             trialv2=(float)r2/(float)RAND_MAX;
/*             printf("%lf %lf\n",(float)r1/(float)RAND_MAX,(float)r2/(float)RAND_MAX); */
             printf("%lf %lf\n",trialv2,mag_d(&trialv2,nsteps,H0,B0));
         }
/*            trialv1=1.9e1;
            trialv2=(float)r2/(float)RAND_MAX;
            printf("%lf %lf\n",pdtct(&trialv1),size_d(&trialv2));
 */
        }

