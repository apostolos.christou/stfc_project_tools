      #include"ephem.h"
   
     struct planet earth2=
      {
        { 398600.44},
        
          {1.00000011, 0.01671022, 0.00005, -11.26064,
           102.94719,  100.46435},
           
          {-0.00000005, -0.00003804, -46.94, -18228.25,
            1198.28, 129597740.63}
	    };      
       
     struct vector solvec(double *jd)
     {
     double prec[9];
     static struct vector p;
     static struct vector s; 
     
     p=el2pos(jd,earth2);
     ecl2eq(prec);
     s.x=(-p.x)*prec[0] + (-p.y)*prec[1] + (-p.z)*prec[2];
     s.y=(-p.x)*prec[3] + (-p.y)*prec[4] + (-p.z)*prec[5];
     s.z=(-p.x)*prec[6] + (-p.y)*prec[7] + (-p.z)*prec[8]; 
     
     return(s);
     }

