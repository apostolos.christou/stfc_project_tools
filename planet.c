
      #include  "ephem.h"                      


     
     void ecl2eq(double *tmat)
      
       /* TRANSFORMS ECLIPTIC TO EQUATORIAL COORDINATES */
       
      {
      
      double eps=-(23.0*D2R + 26.0*M2R + 21.4119*S2R);
      
      tmat[0]=1.0;
      tmat[1]=0.0;
      tmat[2]=0.0;
      tmat[3]=0.0;
      tmat[4]=cos((eps));
      tmat[5]=sin((eps));
      tmat[6]=0.0;
      tmat[7]=-sin((eps));
      tmat[8]=cos((eps));
      
      }
                                                         
      struct elements update(double *time, struct planet body)
      /* This subroutine updates orbital elements for the epoch */
      {      
      
      double t;
      struct elements derivs,result;
      
      t=((*time) - J2000)/JULCEN;
       
      derivs.a=body.del.da;
      derivs.e=body.del.de;
      derivs.i=body.del.di;
      derivs.lan=body.del.dlan;
      derivs.lper=body.del.dlper;
      derivs.ml=body.del.dml;
       
       
      result.a=body.el.a + t*derivs.a;
      result.e=body.el.e + t*derivs.e;
      result.i=body.el.i + t*derivs.i/3.6e3;
      result.lan=body.el.lan + t*derivs.lan/3.6e3;
      result.lper=body.el.lper + t*derivs.lper/3.6e3;
      result.ml=body.el.ml + t*derivs.ml/3.6e3;      
       
      return(result);
      
      }

      
      
      
      double dkeplr(double *x, double *ecc)
/*    This subroutine calculates the value of the 1st derivative of
      x-esinx-M.  */
      {
      return(1.0-(*ecc)*cos((*x)));   
      }    
      
      
      
      double kepler(double *x, double *ma, double *ecc)

 /*  This subroutine calculates the value of x-esinx-M.  */    
      {
      return((*x)-(*ecc)*sin((*x))-(*ma));
      }
      
      
     double  m2e(double *ma, double *ecc)
     /* SOLVES KEPLER'S EQUATION WITH NEWTON-RAPHSON METHOD  */                                               
     {                                              
     int niter;
     double x;
                                                                 
     niter=0;
     x=(*ma);
     
     while( niter < IMAX  &&  fabs(kepler(&x,ma,ecc)) > TOL)
        {
        x+=(-kepler(&x,ma,ecc)/dkeplr(&x,ecc));
       niter+=1;   
       } 
     return(x);
     }
      
     double e2t(double *ecan, double *ecc)
     {
     /* CONVERTS ECCENTRIC ANOMALY TO TRUE ANOMALY */
     
     double  denom,cosf,sinf;
     
     denom=1.0- (*ecc)*cos((*ecan));
     cosf=(cos((*ecan))-(*ecc))/denom;
     sinf=sqrt(1.0 - (*ecc)*(*ecc))*sin((*ecan))/denom;
     return(atan2(sinf,cosf));
     }                          
          
          
     double m2t(double *time, struct planet body)
     {  
     /* COMPUTES TRUE ANOMALY FOR THE EPOCH */
     static double t,ma,ecan;
                    
     t=((*time) - J2000)/JULCEN;               
     ma=(body.el.ml-body.el.lper)/R2D;
       
     ecan=m2e(&ma, &(body.el.e)); 
     /* printf("%8.6f %8.6f %8.6f \n", t,ma,ecan); */
     return(e2t(&ecan, &(body.el.e)));
     }
                               
     double radius(double *time, struct planet body)
     {   
     double ta;
     
     ta=m2t(time, body);
     return(body.el.a*(1.0 - body.el.e*body.el.e)/(1.0 + body.el.e*cos(ta)));
     }
     
     struct vector el2pos(double *time, struct planet body)
     {
     /* CONVERTS ELEMENTS TO X,Y,Z AT EPOCH*/
     
     double cosi,sini,coslan,sinlan,costl,sintl,r;
     struct vector ptn;
     struct planet nubody;    
     static double tran;
     
     nubody.phys.gm=body.phys.gm;
     
     nubody.del.da=body.del.da;
     nubody.del.de=body.del.de;
     nubody.del.di=body.del.di;
     nubody.del.dlan=body.del.dlan;
     nubody.del.dlper=body.del.dlper;
     nubody.del.dml=body.del.dml;
     
     nubody.el=update(time,body);
     
     cosi=cos(nubody.el.i/R2D);
     sini=sin(nubody.el.i/R2D);
     coslan=cos(nubody.el.lan/R2D);
     sinlan=sin(nubody.el.lan/R2D);
     tran=m2t(time, nubody);
     costl=cos((tran*R2D + nubody.el.lper -nubody.el.lan)/R2D);
     sintl=sin((tran*R2D + nubody.el.lper -nubody.el.lan)/R2D);
         
     r=radius(time,nubody);
         
     ptn.x=r*(costl*coslan - sintl*sinlan*cosi);
     ptn.y=r*(costl*sinlan + sintl*coslan*cosi);
     ptn.z=r*sintl*sini;
     return(ptn);
     }
     
struct vector vel_ephem(double *time, double dt, struct planet body)

{
    static double t,A,B;
    struct vector bplus, bminus;
    
    t=(*time)+dt;
    bplus=el2pos(&t,body);
    t+=(-2.0*dt);
    bminus=el2pos(&t,body);
    A=0.5/dt;
    B=-A;
    return(lincomb(A,bplus,B,bminus));
}

