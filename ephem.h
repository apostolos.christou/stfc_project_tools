      #include <stdlib.h>
      #include <stdio.h>
      #include <time.h>
      #include <stddef.h>
      #include <string.h>
      #include <math.h>      

      #define  IMAX            20
      #define  TOL             1.0e-8  
      #define  PI              3.14159265358979323846e0
      #define  R2D             57.295779512
      #define  D2R             1.745329252e-2
      #define  M2R             2.908882087e-4
      #define  S2R             4.848136811e-6
      #define  J2000           2.451545e6    
      #define  JULCEN          3.6525e4    
      #define  ONEAU           1.4959787e8 
      #define  CAP_G           1.5e-1

      #define  V0              2.1e1
      #define  W0              2e-1
      #define  H0              18.1e0
      #define  H00             16.1e0
      #define  HMAX            23e0
      #define  A0             -8.05954e0 /* w/o Eureka */
      #define  B0              0.445135
      #define  A00            -4.9779e0 /* w Eureka */
      #define  B00             0.292221e0            
      #define  C1             -0.955262 /* w/o Eureka & subtr 16.1*/
      #define  D1              0.468873
      #define  E1             -0.490283 /* data from Pravec et al (2018) */
      #define  F1              0.564128

     struct spherical
               {
               double r;
               double lmda;
               double phi;
               };       

     struct vector{
                    double x;
                    double y;
                    double z;
                   };

     struct state{
                 struct vector pos;
                 struct vector vel;
                 };                                        
                    
      struct physical { 
                  double gm;
                     };
                     
      
      struct elements {
                 double a;
                 double e;
                 double i;
                 double lan;
                 double lper;
                 double ml;
                      };
                
       struct rates {
                 double da;
                 double de;
                 double di;
                 double dlan;
                 double dlper;
                 double dml; 
                    };        
                  
      struct planet{  
             struct physical phys;
             struct elements el;
             struct rates del;
                   };



      double date1();
      double date2(int y);
      double julian(int Year, int Month, int Day,
                    int Hour, int Minutes, int Seconds); 
      void calendar(double jd, int *year, int *month, int *day);

      void corr(double *time, double *tmat);             
      void   ecl2eq(double *tmat);                                   
      struct elements update(double *time, struct planet body);
      double dkeplr(double *x, double *ecc);                     
      double kepler(double *x, double *ma, double *ecc);
      double m2e(double *ma, double *ecc);
      double e2t(double *ecan, double *ecc);  
      double m2t(double *time, struct planet body);
      double radius(double *time, struct planet body);
      struct vector lincomb(double a, struct vector v1, double b, struct vector v2);
      struct vector add(struct vector v1,struct vector v2);
      struct vector subtract(struct vector v1,struct vector v2);
      double dot(struct vector vec1, struct vector vec2);
      double norm2(struct vector vec);
      struct vector cross(struct vector vec1, struct vector vec2);
      void BubbleSort(double a[], int array_size);
      struct vector el2pos(double *time, struct planet body);
      struct vector solvec(double *jd);
      struct vector sph2xyz(struct spherical rlf);
      struct spherical luneph(double *time);
      struct vector lunpos(double *tt); 
      struct vector vel_ephem(double *time, double dt, struct planet body);
      double pdtct(double *v);
      double phase(double *time, struct planet asteroid, struct planet observer);
      double elong(double *time, struct planet asteroid, struct planet observer);
      double vmag(double *time, double *phase, struct planet asteroid, struct planet observer);
      double mag_d(double *x, double nast, double HLR, double K0);
      double poisson_k(double rate, int k);
