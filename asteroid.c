      #include  "ephem.h"   

double pdtct(double *v)
 {
  return(1e0/(1e0+exp(((*v)-V0)/W0)));
 }

double phase(double *time, struct planet asteroid, struct planet observer)
 {
  return(R2D*acos(dot(el2pos(time,asteroid),subtract(el2pos(time,asteroid),	\
      el2pos(time,observer)))/sqrt(norm2(subtract(el2pos(time,asteroid), \
      el2pos(time,observer)))*norm2(el2pos(time,asteroid)))));
 }

double elong(double *time, struct planet asteroid, struct planet observer)
{
  return(R2D*acos(dot(el2pos(time,observer),subtract(el2pos(time,observer),     \
	  el2pos(time,asteroid)))/sqrt(norm2(subtract(el2pos(time,asteroid), \
          el2pos(time,observer)))*norm2(el2pos(time,observer)))));
}

double vmag(double *time, double *phase, struct planet asteroid, struct planet observer)
        {
	  return(2.5*log10(norm2(el2pos(time,asteroid))\
          *norm2(subtract(el2pos(time,asteroid),\
                          el2pos(time,observer)))) - \
	  2.5*log10((1-CAP_G)*(exp(-3.33*pow(tan(0.5*(*phase)/R2D),\
          0.63)) + CAP_G*(exp(-1.87*pow(tan(0.5*(*phase/R2D)),1.22))))));
            /* from  Muinonen, K. et al, A three-parameter magnitude phase 
               function for asteroids. Icarus, 2010, 209, 542-555
             */
            /* from   -2.5*log10((1-CAP_G)*(exp(-3.33*pow(tan(0.5*phase),0.63)) + CAP_G*(exp(-1.87*pow(tan(0.5*phase),1.22))
             */
            /* see also Yoshida & Terai, 2018  */
        }
   double mag_d(double *x, double nast, double HLR, double K0)
   {
     return(HLR + log10(1e0+(*x)*(nast))/K0);
   }

double poisson_k(double rate, int k)
/* probability of at least k detections at poisson rate "rate" */
{
  static int i;
  static double term, sum;
  
  sum=1e0;
  for(i=1;i<k;i++)
    {
     term*=rate/(double)i;
     sum+=term;
    }
  return(1e0 - exp(-rate)*sum);
}

